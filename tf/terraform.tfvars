tags = {
      terraform = "true"
}

vpc_id = "vpc-0e81037def08edd91"
default_public_subnets_cidr=["172.31.0.0/20","172.31.16.0/20","172.31.32.0/20"]
desired_private_subnets_cidr=["172.31.48.0/28","172.31.48.16/28","172.31.48.32/28"]
s3bucket_name = "test-bucket-gt-777722900107"