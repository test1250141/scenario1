variable "tags" {
    type = map 
    default = {}
}

variable "vpc_id"{}
variable "s3bucket_name"{}
variable "default_public_subnets_cidr"{
  type = set(string)
}
variable "desired_private_subnets_cidr"{
  type = set(string)
}
variable "ami"{
    default = "ami-0fa7190e664488b99" #amzn2
}
variable "key_name"{
    default = "ec2-instance-key"
}
variable "instance_type"{
    default = "t2.micro"
}