################################################################
# Assumptions:
# Default VPC has 3 subnets, all using same RT
# Default VPC CIDR - 172.31.0.0/16
# Default subnet CIDR - 172.31.16.0/20 (apse1-az1), 
# 172.31.32.0/20 (apse1-az2), 172.31.0.0/20 (apse1-az3)
# RT has a route to internet via default IGW
# DNS hostnames of default VPC is disabled
################################################################
# VPC id must be provided
# default public subnets cidr must be provided
# desired private subnets cidr must be provided
# User that runs this tf file has all required permissions
################################################################


data "aws_vpc" "vpc" {
   id = var.vpc_id 
}

data "aws_subnet" "default_public_subnets" {

  for_each = var.default_public_subnets_cidr
  cidr_block = each.value
}


##############################################################
#  Create private subnets
##############################################################

resource "aws_subnet" "private_subnets" {

  vpc_id     = data.aws_vpc.vpc.id
  for_each = zipmap(["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"], tolist(var.desired_private_subnets_cidr))
  cidr_block = each.value
  availability_zone = each.key

  tags = { type = "private" }
}


##############################################################
#  CLOUDWATCH logging
##############################################################

resource "aws_iam_role" "vpcfl" {
  name = "iamrole-s1-vpcfl"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "vpcfl-rp" {
  name = "vpcfl-policy"
  role = aws_iam_role.vpcfl.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_flow_log" "awsfl" {
  iam_role_arn    = aws_iam_role.vpcfl.arn
  log_destination = aws_cloudwatch_log_group.cwlg.arn
  traffic_type    = "ALL"
  vpc_id          = data.aws_vpc.vpc.id
}

resource "aws_cloudwatch_log_group" "cwlg" {
  name = "/vpc/default"
}

################################################################
# S3 and gateway endpoint
################################################################


resource "aws_s3_bucket" "s3bucket" {
  bucket = var.s3bucket_name
}

resource "aws_s3_object" "index" {
  bucket = var.s3bucket_name
  key    = "/web/index.html"
  source = "../web/index.html"
 depends_on = [
    aws_s3_bucket.s3bucket
  ]
}
resource "aws_s3_object" "js" {
  bucket = var.s3bucket_name
  key    = "/web/js/s1.js"
  source = "../web/js/s1.js"
 depends_on = [
    aws_s3_bucket.s3bucket
  ]
}
resource "aws_s3_object" "css" {
  bucket = var.s3bucket_name
  key    = "/web/css/s1.css"
  source = "../web/css/s1.css"
 depends_on = [
    aws_s3_bucket.s3bucket
  ]
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id       = data.aws_vpc.vpc.id
  service_name = "com.amazonaws.ap-southeast-1.s3"
}


#############################################
# EIP and NAT gateway
#############################################
resource "aws_eip" "eip" {
}

resource "aws_nat_gateway" "natgw" {

  allocation_id = aws_eip.eip.id
  
  #any public subnet will do
  subnet_id     = values(data.aws_subnet.default_public_subnets).*.id[0]

}

################################################################
# Route table for private subnet
# Private Subnet -> NAT (Public) Subnet -> Internet (IGW)
# [NAT Subnet -> Internet]
# Create a RT and adds a route to Internet through NAT
# Destination   |     Target
# <VPC-CIDR>    |     local
# 0.0.0.0/0     |     <NAT-ID>
################################################################
resource "aws_route_table" "private-rt" {
  vpc_id = data.aws_vpc.vpc.id

  route {
    cidr_block = data.aws_vpc.vpc.cidr_block
    gateway_id = "local"
  }
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgw.id
  }
}

################################################################
# Associates route table to private subnets
################################################################
resource "aws_route_table_association" "rt-a" {

  for_each = aws_subnet.private_subnets
  subnet_id = aws_subnet.private_subnets[each.key].id
  route_table_id = aws_route_table.private-rt.id

}


##############################################################
# instance keypair 
##############################################################
resource "tls_private_key" "tls_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.tls_key.public_key_openssh
}

output "private_key" {
  value     = tls_private_key.tls_key.private_key_openssh
  sensitive = true
}

resource "local_file" "pvk" {
    content  = tls_private_key.tls_key.private_key_openssh
    filename = "${var.key_name}.pem"
}

################################################################
#  SSM for ec2 connect
################################################################
resource "aws_iam_role" "ssm_role" {
  name = "iamrole-s1-ssm"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
   managed_policy_arns = [ "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore", "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"]
}

resource "aws_iam_instance_profile" "ssm_profile" {
  name = "s1_ssm_profile"
  role = aws_iam_role.ssm_role.name
}

################################################################
# Create ALB, ALB SecGrp and forward to listener (Target group)
# Create target group for ALB listener
################################################################
resource "aws_lb" "alb" {
  name               = "web-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.albsg.id]

  #subnet[1] and subnet[2] will be used as they are in public subnet
  subnets            =  flatten(values(data.aws_subnet.default_public_subnets).*.id) 

}
resource "aws_security_group" "albsg" {
  name        = "sgrp-alb"
  description = "Allow Traffic"
  vpc_id      =  data.aws_vpc.vpc.id

  ingress {
    description      = "SSH, HTTP"
    from_port        = 22
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
}

resource "aws_lb_target_group" "tg" {
  name     = "WEB-TargetGroup"
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.vpc.id
}

#############################################
# Create launch template and ASG
#############################################

resource "aws_security_group" "asgsg" {
  name        = "sgrp-asgsg"
  description = "Allow Traffic"
  vpc_id      = data.aws_vpc.vpc.id

  #Testing - [ Attach EIP and SSH from local ] 
  #ingress {
  #  description      = "SSH, HTTP, HTTPS"
  #  from_port        = 22
  #  to_port          = 443
  #  protocol         = "tcp"
  #  cidr_blocks      = ["0.0.0.0/0"]
  #}
  
  ingress {
    description      = "Traffic within VPC"
    from_port        = 0
    to_port          = 65535
    protocol         = "tcp"
    cidr_blocks      = [data.aws_vpc.vpc.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {Name = "sgrp-asgsg" }
  
}

data "template_file" "user_data" {
  template = "${file("user_data.tpl")}"

  vars = {
    bucket = var.s3bucket_name
  }
}

resource "aws_launch_template" "launch_template" {
  name = "asg_lt"
  iam_instance_profile {
    name = aws_iam_instance_profile.ssm_profile.name
  }

  image_id = var.ami
  instance_type = var.instance_type
  key_name  = var.key_name
  vpc_security_group_ids =  [aws_security_group.asgsg.id]
  user_data = base64encode(data.template_file.user_data.rendered)
}


resource "aws_autoscaling_group" "asg" {

   depends_on = [
    aws_lb.alb
  ]

  vpc_zone_identifier = flatten(values(aws_subnet.private_subnets).*.id)
  desired_capacity   = 3
  max_size           = 3
  min_size           = 3
  launch_template {
    id      = aws_launch_template.launch_template.id
    version = aws_launch_template.launch_template.latest_version
  }

  target_group_arns =[ aws_lb_target_group.tg.arn]
   instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }

  }

    tag {
    key                 = "tier"
    value               = "web"
    propagate_at_launch = true
  }
}
