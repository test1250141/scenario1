#!/bin/bash

exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

sudo su -
sudo yum update -y
sudo amazon-linux-extras install nginx1 -y
sudo amazon-linux-extras install nginx1 -y
aws s3 cp s3://${bucket} . --recursive
mv -f web/index.html /usr/share/nginx/html/index.html
mkdir -p /usr/share/nginx/html/js
mv -f web/js/s1.js /usr/share/nginx/html/js/s1.js
mkdir -p /usr/share/nginx/html/css
mv -f web/css/s1.css /usr/share/nginx/html/css/s1.css

sudo systemctl start nginx
sudo systemctl enable nginx
sudo systemctl status nginx