# Improvement

### WAF
>Apply to ALB or APIGW.  Provides security from common web attacks and allow customized rules.
### NLB
>Static ip for whitelisting from other services, infront of ALB
### TLS on 443
> Encrypts data in transit. Provides server authentication verify identity.
### Caching
> With CloudFront or Redis to reduce load and improve latency.
### KMS 
> Use CMK for S3 encryption. Maintain greater control over data security vs managed. Compliance.
### IAM
> Ensure the ec2 instance profile role follows the principle of least privilege.
### Backup and DR
> With AWS Backup
### HA and S3 Versioning
> Multi AZ
### APIGW
> Provides loggings, throttling, authentication control, multiple stage for blue/green
### Containerized 
> Cheaper
### NACL and SG
> Only allow traffic on required ports 
